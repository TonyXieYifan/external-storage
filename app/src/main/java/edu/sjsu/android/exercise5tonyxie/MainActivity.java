package edu.sjsu.android.exercise5tonyxie;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import edu.sjsu.android.exercise5tonyxie.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String FILE_TYPE = Environment.DIRECTORY_DOWNLOADS;
    private static final int REQUEST_CODE = 100;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    private File getPath() {
        // If check box checked, store to shared storage
        if (binding.share.isChecked())
            return Environment.getExternalStoragePublicDirectory(FILE_TYPE);
        // If check box unchecked, store to app-specific storage
        else return this.getExternalFilesDir(FILE_TYPE);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // If user denied the permission
        if (requestCode == REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // If user chose "Deny and never ask again"
            if (!shouldShowRequestPermissionRationale(permissions[0])) {
                Toast.makeText(this, "Permission denied",
                        Toast.LENGTH_SHORT).show();
            }else { // If user chose "Deny"
                Toast.makeText(this, "Please give writing permission",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void writeFile(View v) {
        String name = binding.nameW.getText().toString();
        String content = binding.content.getText().toString();
        if (checkPermission()) {
            File path = getPath();
            // If successfully wrote
            if (FileOperations.write(path, name, content))
                Toast.makeText(this, name + " created",
                        Toast.LENGTH_SHORT).show();
            // If anything wrong
            else
                Toast.makeText(this, "I/O error",
                        Toast.LENGTH_SHORT).show();
        }else requestPermission();
    }

    public void readFile(View v) {
        String name = binding.nameR.getText().toString();
        File path = getPath();
        String text = FileOperations.read(path, name);
        // If read nothing, means something wrong
        if (text == null) {
            Toast.makeText(this, "File not Found",
                    Toast.LENGTH_SHORT).show();
        }
        binding.result.setText(text);
    }

    public void uninstall(View v) {
        Intent delete = new Intent(Intent.ACTION_DELETE,
                Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }
}