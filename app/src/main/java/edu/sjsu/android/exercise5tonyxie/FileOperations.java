package edu.sjsu.android.exercise5tonyxie;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileOperations {

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static Boolean write(File path, String name, String content) {
        // Return false if external storage is not available
        if (!isExternalStorageWritable()) return false;
        // Don't know the file size, so wrap in try/catch
        try {
            File file = new File(path, name);
            // Can use other Java.io classes to write the file
            // New content will replace existing content if file exists
            FileWriter writer = new FileWriter(file);
            writer.write(content);
            writer.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static String read(File path, String name) {
        try {
            StringBuilder output = new StringBuilder();
            File file = new File(path, name);
            // Can use other Java.io classes to read the file
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = "";
            while ((line = br.readLine()) != null) {
                output.append(line).append("\n");
            }
            return output.toString();
        }catch (IOException e) {
            return null;
        }
    }
}
